/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.core.simplifierrules;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.resource.Code;

@RunWith(JUnit4.class)
public class SimplifierParserTest {
	private Code code;
	
	@Before
	public void setUp() {
		code = new Code();
	}
	
	@Test
	public void flagRuleOnly() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if ADDABLE or DELETABLE or CLONEABLE;", code);
		parser.parse();
	}
	
	@Test
	public void flagRuleAndData() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if ADDABLE and DELETABLE and CLONEABLE and DATA = \"test\";", code);
		parser.parse();
	}
	
	@Test
	public void flagRuleAndDataWithParens() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if (ADDABLE and DELETABLE and CLONEABLE) and (DATA = \"test\");", code);
		parser.parse();
	}
	
	@Test
	public void allFields() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if DATA = \"test\" and OUTER_DATA = \"test\" and ORIGINAL_ID = \"test\" and TYPE = \"test\";", code);
		parser.parse();
	}
	
	@Test
	public void allFieldsAndTagType() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if (DATA = \"test\" and OUTER_DATA = \"test\" and ORIGINAL_ID = \"test\" and TYPE = \"test\") or (TAG_TYPE = OPENING);", code);
		parser.parse();
	}
	
	@Test
	public void simpleRule() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if ADDABLE;", code);
		parser.parse();
	}
	
	@Test
	public void match() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if DATA ~ \"xtest\";", code);
		parser.parse();
	}
	
	@Test
	public void notMatch() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if DATA !~ \"test\";", code);
		parser.parse();
	}
	
	@Test
	public void equals() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if TYPE = \"test\";", code);
		parser.parse();
	}
	
	@Test
	public void notEqual() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if TYPE != \"test\";", code);
		parser.parse();
	}
	
	@Test
	public void embeddedExpressions() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if TYPE != \"test\" and (DATA = \"one\" or (DATA = \"two\" and DATA = \"three\"));", code);
		parser.parse();		
	}
	
	@Test
	public void comments() throws ParseException {
		SimplifierRules parser = new SimplifierRules("/* test\n line two */ #TAG != \"test\"\nif TYPE != \"test\";", code);
		parser.parse();
	}
	
	@Test
	public void manyRules() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if TYPE != \"test\";"
				+ "\nif DATA ~ \"x\" or (ADDABLE);"
				+ "\nif ADDABLE and CLONEABLE and DELETABLE;", code);
		parser.parse();
	}
	
	@Test(expected=ParseException.class)
	public void emptyRule() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if ();", code);
		parser.parse();
	}
	
	@Test(expected=ParseException.class)
	public void withoutSemiColon() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if ADDABLE", code);
		parser.parse();
	}
	
	@Test(expected=ParseException.class)
	public void missingParen() throws ParseException {
		SimplifierRules parser = new SimplifierRules("if ADDABLE);", code);
		parser.parse();
	}
	
	@Test(expected=ParseException.class)
	public void extraParen() throws ParseException {
		SimplifierRules parser = new SimplifierRules("(if ADDABLE));", code);
		parser.parse();
	}
	
	@Test(expected=ParseException.class)
	public void mispelledField() throws ParseException {
		SimplifierRules parser = new SimplifierRules("(if DATAA = \"null\");", code);
		parser.parse();
	}
}
