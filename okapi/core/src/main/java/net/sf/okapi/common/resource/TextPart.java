/*===========================================================================
  Copyright (C) 2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.resource;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Set;

import net.sf.okapi.common.annotation.Annotations;
import net.sf.okapi.common.annotation.IAnnotation;

/**
 * Implements the base object for the parts that make up a content.
 */
public class TextPart implements IWithProperties, IWithAnnotations {
	protected LinkedHashMap<String, Property> properties;
	protected Annotations annotations;

	/**
	 * Text fragment of this part.
	 */
	public TextFragment text;

	/**
	 * Creates an empty part.
	 */
	public TextPart() {
		text = new TextFragment();
	}

	/**
	 * Creates a new TextPart with a given {@link TextFragment}.
	 * 
	 * @param text
	 *            the {@link TextFragment} for this new part.
	 */
	public TextPart(TextFragment text) {
		if (text == null) {
			text = new TextFragment();
		}
		this.text = text;
	}

	/**
	 * Creates a new TextPart with a given text string.
	 * 
	 * @param text
	 *            the text for this new part.
	 */
	public TextPart(String text) {
		this.text = new TextFragment(text);
	}

	@Override
	public TextPart clone() {
		TextPart tp = new TextPart(text.clone());
		// Clone the properties
		if ( this.properties != null ) {
			tp.properties = new LinkedHashMap<String, Property>();
			for ( Property prop : this.properties.values() ) {
				tp.properties.put(prop.getName(), prop.clone()); 
			}
		}
		// clone annotations
		if (annotations != null) {
			tp.annotations = annotations.clone();
		}
		return tp;
	}

	@Override
	public String toString() {
		if (text == null)
			return "";
		return text.toText();
	}

	/**
	 * Gets the text fragment for this part.
	 * 
	 * @return the text fragment for this part.
	 */
	public TextFragment getContent() {
		return text;
	}

	/**
	 * Sets the {@link TextFragment} for this part.
	 * 
	 * @param fragment
	 *            the {@link TextFragment} to assign to this part. It must not
	 *            be null.
	 */
	public void setContent(TextFragment fragment) {
		this.text = fragment;
	}

	/**
	 * Indicates if this part is a {@link Segment}.
	 * 
	 * @return true if the part is a {@link Segment}, false if it is not.
	 */
	public boolean isSegment() {
		return false;
	}

	@Override
	public Iterable<IAnnotation> getAnnotations() {
		if (annotations == null) {
			return Collections.emptyList();
		}
		return annotations;
	}

	@Override
	public <A extends IAnnotation> A getAnnotation(Class<A> annotationType) {
		if (annotations == null)
			return null;
		return annotationType.cast(annotations.get(annotationType));
	}

	@Override
	public void setAnnotation(IAnnotation annotation) {
		if (annotations == null)
			annotations = new Annotations();
		annotations.set(annotation);
	}

	@Override
	public Property getProperty(String name) {
		if (properties == null)
			return null;
		return properties.get(name);
	}

	@Override
	public Property setProperty(Property property) {
		if (properties == null)
			properties = new LinkedHashMap<String, Property>();
		properties.put(property.getName(), property);
		return property;
	}

	@Override
	public void removeProperty(String name) {
		if (properties != null) {
			properties.remove(name);
		}
	}

	@Override
	public Set<String> getPropertyNames() {
		if (properties == null)
			properties = new LinkedHashMap<String, Property>();
		return properties.keySet();
	}

	@Override
	public boolean hasProperty(String name) {
		if (properties == null)
			return false;
		return properties.containsKey(name);
	}
}
