package net.sf.okapi.common.resource;

import net.sf.okapi.common.annotation.IAnnotation;

public interface IWithAnnotations {
	/**
	 * Gets the annotation object for a given class for this resource.
	 * @param <A> the type of the class.
	 * @param annotationType the class of the annotation object to retrieve.
	 * @return the annotation for the given class for this resource. 
	 */
	public <A extends IAnnotation> A getAnnotation(Class<A> annotationType);

	/**
	 * Sets an annotation object for this resource.
	 * @param annotation the annotation object to set.
	 */
	public void setAnnotation (IAnnotation annotation);

	/**
	 * Gets the iterable list of the annotations for this resource.
	 * @return the iterable list of the annotations for this resource.
	 */
	public Iterable<IAnnotation> getAnnotations ();
}
