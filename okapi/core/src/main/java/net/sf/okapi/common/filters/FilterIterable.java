package net.sf.okapi.common.filters;

import java.util.Iterator;
//jdk8 import java.util.stream.Stream;
//jdk8 import java.util.stream.StreamSupport;

import net.sf.okapi.common.Event;

/**
 * Wrapper class that takes an IFilter and makes it Iterable. 
 */
public class FilterIterable implements Iterable<Event> {
	private final IFilter filter;

	public FilterIterable(final IFilter filter) {
		this.filter = filter;
	}

	@Override
	public final Iterator<Event> iterator() {
		return new Iterator<Event>() {

			@Override
			public boolean hasNext() {
				return filter.hasNext();
			}

			@Override
			public Event next() {
				return filter.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException(
						"Removing Event(s) from IFilter is not supported");
			}
		};
	}

/*
 * TODO(jdk8): we can't do this until we stop supporting JDK 7
	public final Stream<Event> stream() {
		return StreamSupport.stream(spliterator(), false);
	}
*/
}
