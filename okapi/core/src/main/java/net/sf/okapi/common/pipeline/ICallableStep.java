package net.sf.okapi.common.pipeline;

import java.util.concurrent.Callable;

import net.sf.okapi.common.Event;

/**
 * Step that implements the Callable interface and can be run concurrently.
 *
 * @param <T> - return type of call method, i.e. Event, SortableEvent
 */
public interface ICallableStep<T> extends IPipelineStep, Callable<T> {
	
	/**
	 * Gets the main step wrapped by ICallableStep
	 * @return the main step
	 */
	public IPipelineStep getMainStep();
	
	/**
	 * process the event now without threading. Normally used for initialization 
	 * events such as START_BATCH etc.
	 * @param event - event to be processed
	 * @return the resulting event
	 */
	public Event processNow(Event event);
}
