/*===========================================================================
  Copyright (C) 2010-2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.microsoft;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String AZUREKEY = "azureKey";
	private static final String CATEGORY = "category";
	
	public Parameters () {
		super();
	}

	public String getAzureKey() {
		return getString(AZUREKEY);
	}

	public void setAzureKey(String azureKey) {
		setString(AZUREKEY, azureKey);
	}

	public String getCategory () {
		return getString(CATEGORY);
	}

	public void setCategory (String category) {
		setString(CATEGORY, category == null ? "" : category);
	}

	@Override
	public void reset () {
		super.reset();
		setAzureKey("");
		setCategory("");
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(AZUREKEY,
			"Azure Key (See https://translatorbusiness.uservoice.com/knowledgebase/articles/1078534-microsoft-translator-on-azure#signup)",
			"Microsoft Azure subscription key");
		desc.add(CATEGORY,
			"Category (See http://hub.microsofttranslator.com", "A category code for an MT system trained by user data, if any");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Microsoft MT Connector Settings", true, false);
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(AZUREKEY));
		tip.setPassword(true);
		tip = desc.addTextInputPart(paramsDesc.get(CATEGORY));
		tip.setAllowEmpty(true);
		return desc;
	}

}
