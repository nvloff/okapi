/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.promt;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	protected static final String HOST = "host";
	protected static final String USERNAME = "username";
	protected static final String PASSWORD = "password";
	
	public String getHost () {
		return getString(HOST);
	}

	public void setHost (String host) {
		setString(HOST, host);
	}

	public String getUsername () {
		return getString(USERNAME);
	}

	public void setUsername (String username) {
		setString(USERNAME, username);
	}

	public String getPassword () {
		return getString(PASSWORD);
	}

	public void setPassword (String password) {
		setString(PASSWORD, password);
	}

	public Parameters () {
		super();
	}
	
	public Parameters (String initialData) {
		fromString(initialData);
	}
	
	public void reset () {
		super.reset();
		setHost("ptsdemo.promt.ru/");
		setUsername("");
		setPassword("");
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(HOST, "Host server", "The root URL of the host server (e.g. http://ptsdemo.promt.ru/");
		desc.add(USERNAME, "User name (optional)", "The login name to use");
		desc.add(PASSWORD, "Password (if needed)", "The password for the given user name");
		return desc;
	}

	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("ProMT Connector Settings");
		desc.addTextInputPart(paramsDesc.get(HOST));
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(USERNAME));
		tip.setAllowEmpty(true); // Username is optional
		tip = desc.addTextInputPart(paramsDesc.get(PASSWORD));
		tip.setPassword(true);
		tip.setAllowEmpty(true); // Password is optional
		return desc;
	}

}
