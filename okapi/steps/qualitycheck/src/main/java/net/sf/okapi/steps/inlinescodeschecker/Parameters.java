/*===========================================================================
 Copyright (C) 2016 by the Okapi Framework contributors
 -----------------------------------------------------------------------------
 This library is free software; you can redistribute it and/or modify it 
 under the terms of the GNU Lesser General Public License as published by 
 the Free Software Foundation; either version 2.1 of the License, or (at 
 your option) any later version.

 This library is distributed in the hope that it will be useful, but 
 WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License 
 along with this library; if not, write to the Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 ===========================================================================*/

package net.sf.okapi.steps.inlinescodeschecker;

import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {

	private static final String CODEDIFFERENCE = "codeDifference";
	private static final String GUESSOPENCLOSE = "guessOpenClose";
	private static final String EXTRACODESALLOWED = "extraCodesAllowed";
	private static final String MISSINGCODESALLOWED = "missingCodesAllowed";
	private static final String TYPESTOIGNORE = "typesToIgnore";

	List<String> extraCodesAllowed;
	List<String> missingCodesAllowed;

	public Parameters() {
		super();
	}

	public String getTypesToIgnore() {
		return getString(TYPESTOIGNORE);
	}

	public void setTypesToIgnore(String typesToIgnore) {
		setString(TYPESTOIGNORE, typesToIgnore);
	}

	public boolean getCodeDifference() {
		return getBoolean(CODEDIFFERENCE);
	}

	public void setCodeDifference(boolean codeDifference) {
		setBoolean(CODEDIFFERENCE, codeDifference);
	}

	public boolean getGuessOpenClose() {
		return getBoolean(GUESSOPENCLOSE);
	}

	public void setGuessOpenClose(boolean guessOpenClose) {
		setBoolean(GUESSOPENCLOSE, guessOpenClose);
	}

	@Override
	public void reset() {
		super.reset();

		setCodeDifference(true);
		setGuessOpenClose(true);

		extraCodesAllowed = new ArrayList<String>();
		missingCodesAllowed = new ArrayList<String>();

		setTypesToIgnore("mrk;x-df-s;");
	}

	@Override
	public void fromString(String data) {
		super.fromString(data);

		// Allowed extra codes
		int count = buffer.getInteger(EXTRACODESALLOWED, 0);
		if (count > 0) {
			extraCodesAllowed.clear();
		}
		for (int i = 0; i < count; i++) {
			extraCodesAllowed.add(buffer.getString(String.format("%s%d", EXTRACODESALLOWED, i), ""));
		}
		// Allowed missing codes
		count = buffer.getInteger(MISSINGCODESALLOWED, 0);
		if (count > 0) {
			missingCodesAllowed.clear();
		}
		for (int i = 0; i < count; i++) {
			missingCodesAllowed.add(buffer.getString(String.format("%s%d", MISSINGCODESALLOWED, i), ""));
		}

	}

	@Override
	public String toString() {
		buffer.setInteger(EXTRACODESALLOWED, extraCodesAllowed.size());
		for (int i = 0; i < extraCodesAllowed.size(); i++) {
			buffer.setString(String.format("%s%d", EXTRACODESALLOWED, i), extraCodesAllowed.get(i));
		}
		buffer.setInteger(MISSINGCODESALLOWED, missingCodesAllowed.size());
		for (int i = 0; i < missingCodesAllowed.size(); i++) {
			buffer.setString(String.format("%s%d", MISSINGCODESALLOWED, i), missingCodesAllowed.get(i));
		}

		return super.toString();
	}
}
