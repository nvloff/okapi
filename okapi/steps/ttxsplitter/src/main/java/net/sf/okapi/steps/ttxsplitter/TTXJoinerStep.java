/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.ttxsplitter;

import java.net.URI;
import java.util.ArrayList;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.exceptions.OkapiBadStepInputException;
import net.sf.okapi.common.pipeline.BasePipelineStep;

@UsingParameters(TTXJoinerParameters.class)
public class TTXJoinerStep extends BasePipelineStep {

	private TTXJoinerParameters params;
//	private boolean done = false;
	private ArrayList<URI> inputList;
	
	public TTXJoinerStep () {
		params = new TTXJoinerParameters();
	}

	@Override
	public String getDescription () {
		return "Rebuilds previously split TTX documents into their original documents. "
			+ "Expects: raw document. Sends back: raw document.";
	}

	@Override
	public String getName() {
		return "TTX Joiner";
	}

	@Override
	public IParameters getParameters () {
		return params;
	}

	@Override
	public void setParameters (final IParameters params) {
		this.params = (TTXJoinerParameters)params;
	}

	@Override
	protected Event handleStartBatch (final Event event) {
//		done = true;
		inputList = new ArrayList<>();
		return event;
	}

//	@Override
//	protected Event handleStartBatchItem (final Event event) {
//		done = false;
//		return event;
//	}
	
	@Override
	protected Event handleRawDocument (final Event event) {
		URI uri = event.getRawDocument().getInputURI();
		if ( uri == null ) {
			throw new OkapiBadStepInputException("TTX Joiner expects URI inputs.");
		}
		inputList.add(uri);
		return event;
	}
	
	@Override
	protected Event handleEndBatch (final Event event) {
		TTXJoiner joiner = new TTXJoiner(params);
		joiner.process(inputList);
		return event;
	}

//	@Override
//	public boolean isDone () {
//		return done;
//	}

}
