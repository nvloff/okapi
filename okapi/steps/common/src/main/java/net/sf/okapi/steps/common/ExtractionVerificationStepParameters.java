/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.common;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.CheckboxPart;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

@EditorFor(ExtractionVerificationStepParameters.class)
public class ExtractionVerificationStepParameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String COMPARESKELETON = "compareSkeleton";
	private static final String STEPENABLED = "stepEnabled";
	private static final String ALLEVENTS = "allEvents";
	private static final String LIMIT = "limit";
	private static final String INTERRUPT = "interrupt";
	
	public boolean monolingual = false;
	
	public ExtractionVerificationStepParameters() {
		super();
	}

	public void reset() {
		super.reset();
		setCompareSkeleton(true);
		setStepEnabled(true);
		setAllEvents(true);
		setLimit(10);
		setInterrupt(false);
	}

	public boolean getStepEnabled () {
		return getBoolean(STEPENABLED);
	}

	public void setStepEnabled (boolean stepEnabled) {
		setBoolean(STEPENABLED, stepEnabled);
	}
	
	public boolean getCompareSkeleton () {
		return getBoolean(COMPARESKELETON);
	}

	public void setCompareSkeleton (boolean compareSkeleton) {
		setBoolean(COMPARESKELETON, compareSkeleton);
	}

	public boolean getAllEvents () {
		return getBoolean(ALLEVENTS);
	}

	public void setAllEvents (boolean allEvents) {
		setBoolean(ALLEVENTS, allEvents);
	}
	
	public int getLimit () {
		return getInteger(LIMIT);
	}
	
	public void setLimit (int limit) {
		setInteger(LIMIT, limit);
	}
	
	public boolean getInterrupt () {
		return getBoolean(INTERRUPT);
	}

	public void setInterrupt (boolean interrupt) {
		setBoolean(INTERRUPT, interrupt);
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(STEPENABLED, "Perform the extraction verification", null);
		desc.add(COMPARESKELETON, "Compare skeleton", null);
		desc.add(ALLEVENTS, "Verify all events (otherwise only text units are verified)", null);
		desc.add(LIMIT, "Maximum number of warnings per document", null);
		desc.add(INTERRUPT, "Interrupt after reaching the maximum number of warnings", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Extraction Verification", true, false);

		CheckboxPart cbp = desc.addCheckboxPart(paramsDesc.get(STEPENABLED));
		desc.addSeparatorPart();
		
		CheckboxPart cbp2 = desc.addCheckboxPart(paramsDesc.get(ALLEVENTS));
		cbp2.setMasterPart(cbp, true);
		
		cbp2 = desc.addCheckboxPart(paramsDesc.get(COMPARESKELETON));
		cbp2.setMasterPart(cbp, true);
		
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(LIMIT));
		tip.setMasterPart(cbp, true);
		
		cbp2 = desc.addCheckboxPart(paramsDesc.get(INTERRUPT));
		cbp2.setMasterPart(cbp, true);
		
		return desc;
	}
}
