/*===========================================================================
  Copyright (C) 2009-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.paraaligner;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {
	private static final String OUTPUT_ONE_TO_ONE_MATCHES_ONLY = "outputOneToOneMatchesOnly";
	private static final String USE_SKELETON_ALIGNMENT = "useSkeletonAlignment";
	
	public Parameters() {
		super();
	}

	public boolean isOutputOneToOneMatchesOnly() {
		return getBoolean(OUTPUT_ONE_TO_ONE_MATCHES_ONLY);
	}

	public void setOutputOneToOneMatchesOnly(boolean outputOneToOneMatchesOnly) {
		setBoolean(OUTPUT_ONE_TO_ONE_MATCHES_ONLY, outputOneToOneMatchesOnly);
	}
	
	public boolean isUseSkeletonAlignment() {
		return getBoolean(USE_SKELETON_ALIGNMENT);
	}

	public void setUseSkeletonAlignment(boolean useSkeletonAlignment) {
		setBoolean(USE_SKELETON_ALIGNMENT, useSkeletonAlignment);
	}

	@Override
	public void reset() {
		super.reset();
		setOutputOneToOneMatchesOnly(true);
		setUseSkeletonAlignment(false);
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(OUTPUT_ONE_TO_ONE_MATCHES_ONLY, "Output 1-1 Matches Only?", 
			"Ouput only 1-1 aligned paragraphs?");		
		desc.add(USE_SKELETON_ALIGNMENT, "Use Skeleton Alignment? (Experimental)", 
				"Use Skeleton alignment? (Experimental)");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Paragraph Aligner", true, false);		
		desc.addCheckboxPart(paramsDesc.get(OUTPUT_ONE_TO_ONE_MATCHES_ONLY));
		desc.addCheckboxPart(paramsDesc.get(USE_SKELETON_ALIGNMENT));
		return desc;
	}
}
