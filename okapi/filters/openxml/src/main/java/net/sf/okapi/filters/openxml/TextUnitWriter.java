package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.resource.TextContainer;

interface TextUnitWriter {
    void write(TextContainer textContainer);
}
