package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;

/**
 * Provides creational parameters.
 */
class CreationalParameters {

    private XMLEventFactory eventFactory;
    private String prefix;
    private String namespaceUri;

    CreationalParameters(XMLEventFactory eventFactory, String prefix, String namespaceUri) {
        this.eventFactory = eventFactory;
        this.prefix = prefix;
        this.namespaceUri = namespaceUri;
    }

    XMLEventFactory getEventFactory() {
        return eventFactory;
    }

    String getPrefix() {
        return prefix;
    }

    String getNamespaceUri() {
        return namespaceUri;
    }
}
