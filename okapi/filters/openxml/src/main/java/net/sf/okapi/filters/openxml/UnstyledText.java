package net.sf.okapi.filters.openxml;

public class UnstyledText implements Textual {
	private String text;

	UnstyledText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return "UnstyledText[" + text + "]";
	}
}
