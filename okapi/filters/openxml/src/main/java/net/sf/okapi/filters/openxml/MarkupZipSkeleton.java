package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.skeleton.ZipSkeleton;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Provide a markup zip skeleton.
 */
class MarkupZipSkeleton extends ZipSkeleton {

    private Markup markup;

    MarkupZipSkeleton(ZipFile original, ZipEntry entry, Markup markup) {
        super(original, entry);
        this.markup = markup;
    }

    Markup getMarkup() {
        return markup;
    }

    public String getModifiedContents() {
        return XMLEventSerializer.serialize(markup);
    }
}
