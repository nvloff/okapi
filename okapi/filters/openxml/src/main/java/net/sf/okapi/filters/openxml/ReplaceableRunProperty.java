package net.sf.okapi.filters.openxml;

/**
 * Provides a replaceable run property interface.
 */
public interface ReplaceableRunProperty {

    /**
     * Checks whether a run property can be replaced by another.
     *
     * @param runProperty A run property to check against
     *
     * @return {@code true} if a property can be replaced
     *         {@code false} otherwise
     */
    boolean canBeReplaced(ReplaceableRunProperty runProperty);

    /**
     * Replaces a run property by another.
     *
     * @param runProperty A run property to replace by
     *
     * @return A replaced run property
     */
    ReplaceableRunProperty replace(ReplaceableRunProperty runProperty);
}
