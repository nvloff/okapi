/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.filters.xliff;

import java.util.HashMap;
import java.util.Map;

/**
 * Helper class that manages ctype attributes for paired tags ({@code <bx>}, {@code <ex>},
 * {@code <bpt>}, {@code <ept>}). The tags use either their {@code id} or {@code rid} attribute
 * for linking the other tag. The value of the {@code rid} attribute should be preferred if present.
 *
 * @author ccudennec
 * @since 26.01.2017
 */
class CodeTypeForPairedTagsHelper {

    public static final String DEFAULT_CODE_TYPE = "Xpt";

    private Map<String, String> codeTypesById = new HashMap<>();
    private Map<String, String> codeTypesByRid = new HashMap<>();

    /**
     * Stores the given {@code ctype} or {@link #DEFAULT_CODE_TYPE} under the given rid or id.
     *
     * @return the ctype or {@link #DEFAULT_CODE_TYPE}
     */
    public String store(String rid, String id, String ctype) {
        String codeType = getNonEmptyCodeType(ctype);
        if (rid != null) {
            codeTypesByRid.put(rid, codeType);
        }
        if (id != null) {
            codeTypesById.put(id, codeType);
        }
        return codeType;
    }

    /**
     * @return the code type stored under the given rid (or id) or {@link #DEFAULT_CODE_TYPE}
     */
    public String find(String rid, String id) {
        String ctype = null;
        if (rid != null && codeTypesByRid.containsKey(rid)) {
            ctype = codeTypesByRid.get(rid);
        }
        else if (id != null && codeTypesById.containsKey(id)) {
            ctype = codeTypesById.get(id);
        }
        return getNonEmptyCodeType(ctype);
    }

    private String getNonEmptyCodeType(String ctype) {
        return ctype != null && ctype.trim().length() > 0 ? ctype : DEFAULT_CODE_TYPE;
    }

}
