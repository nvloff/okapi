/*===========================================================================
  Copyright (C) 2008-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.beans.v2;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filterwriter.GenericFilterWriter;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.lib.beans.v1.LocaleIdBean;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.PersistenceBean;
import net.sf.okapi.lib.persistence.beans.FactoryBean;

public class GenericFilterWriterBean extends PersistenceBean<GenericFilterWriter> {

	private LocaleIdBean locale = new LocaleIdBean();
	private String encoding;
	private FactoryBean skelWriter = new FactoryBean();
//	private EncoderManagerBean encoderManager = new EncoderManagerBean();
	private FactoryBean encoderManager = new FactoryBean();
	
	@Override
	protected GenericFilterWriter createObject(IPersistenceSession session) {
		return new GenericFilterWriter(skelWriter.get(ISkeletonWriter.class, session), 
				encoderManager.get(EncoderManager.class, session));
	}

	@Override
	protected void fromObject(GenericFilterWriter obj, IPersistenceSession session) {
		locale.set(obj.getLocale(), session);
		encoding = obj.getDefEncoding();
		skelWriter.set(obj.getSkeletonWriter(), session);
		encoderManager.set(obj.getEncoderManager(), session);
	}

	@Override
	protected void setObject(GenericFilterWriter obj, IPersistenceSession session) {
		if (locale.getLanguage() != null) {
			obj.setOptions(locale.get(LocaleId.class, session), encoding);
		}		
	}

	public final LocaleIdBean getLocale() {
		return locale;
	}

	public final void setLocale(LocaleIdBean locale) {
		this.locale = locale;
	}

	public final String getEncoding() {
		return encoding;
	}

	public final void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public final FactoryBean getSkelWriter() {
		return skelWriter;
	}

	public final void setSkelWriter(FactoryBean skelWriter) {
		this.skelWriter = skelWriter;
	}

	public final FactoryBean getEncoderManager() {
		return encoderManager;
	}

	public final void setEncoderManager(FactoryBean encoderManager) {
		this.encoderManager = encoderManager;
	}	
}
