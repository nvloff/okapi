/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.verification;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.IssueType;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;

public class GeneralChecker extends AbstractChecker {
	private Pattern patDoubledWords;
	private String doubledWordExceptions;

	@Override
	public void startProcess(LocaleId sourceLocale, LocaleId targetLocale, Parameters params, List<Issue> issues) {
		super.startProcess(sourceLocale, targetLocale, params, issues);

		// Expression for finding doubled words
		// The expression: "\\b(\\w+)\\s+\\1\\b" does not work for extended
		// chars (\w and \s are ASCII based)
		// We have to use the Unicode equivalents
		patDoubledWords = null;
		if (params.getDoubledWord()) {
			patDoubledWords = Pattern.compile("\\b([\\p{Ll}\\p{Lu}\\p{Lt}\\p{Lo}\\p{Nd}]+)[\\t\\n\\f\\r\\p{Z}]+\\1\\b",
					Pattern.CASE_INSENSITIVE);
			// Construct the string of doubled-words that are not errors
			// The working patter is the list like this: ";word1;word2;word3;"
			doubledWordExceptions = ";" + params.getDoubledWordExceptions().toLowerCase() + ";";
		}
	}

	@Override
	public void processStartDocument(StartDocument sd, List<String> sigList) {
		super.processStartDocument(sd, sigList);
	}

	@Override
	public void processStartSubDocument(StartSubDocument ssd) {
		super.processStartSubDocument(ssd);
	}

	@Override
	public void processTextUnit(ITextUnit tu) {
		// Skip non-translatable entries
		if (!tu.isTranslatable()) {
			return;
		}

		// Get the containers
		TextContainer srcCont = tu.getSource();
		TextContainer trgCont = tu.getTarget(getTrgLoc());

		// Check if we have a target
		if (trgCont == null) {
			if (!isMonolingual()) { // No report as error for monolingual files
				// No translation available
				reportIssue(IssueType.MISSING_TARGETTU, tu, null, "Missing translation.", 0, -1, 0, -1,
						Issue.SEVERITY_HIGH, srcCont.toString(), "", null);
				addAnnotation(tu.getSource(), null, IssueType.MISSING_TARGETTU, "Missing translation", 0, -1, 0, -1,
						Issue.SEVERITY_HIGH, null);
			}
			return;
		}

		ISegments srcSegs = srcCont.getSegments();
		ISegments trgSegs = trgCont.getSegments();

		for (Segment srcSeg : srcSegs) {
			Segment trgSeg = trgSegs.get(srcSeg.getId());
			if (trgSeg == null) {
				reportIssue(IssueType.MISSING_TARGETSEG, tu, srcSeg.getId(),
						"The source segment has no corresponding target segment.", 0, -1, 0, -1, Issue.SEVERITY_HIGH,
						srcSeg.toString(), "", null);
				addAnnotation(srcCont, srcSeg.getId(), IssueType.MISSING_TARGETSEG,
						"The source segment has no corresponding target segment.", 0, -1, 0, -1, Issue.SEVERITY_HIGH,
						null);
				continue; // Cannot go further for that segment
			}

			// Check for empty target, if requested
			if (getParams().getEmptyTarget()) {
				if (trgSeg.text.isEmpty() && !srcSeg.text.isEmpty()) {
					reportIssue(IssueType.EMPTY_TARGETSEG, tu, srcSeg.getId(),
							"The target segment is empty, but its source is not empty.", 0, -1, 0, -1,
							Issue.SEVERITY_HIGH, srcSeg.toString(), "", null);
					addAnnotation(srcCont, srcSeg.getId(), IssueType.EMPTY_TARGETSEG,
							"The target segment is empty, but its source is not empty.", 0, -1, 0, -1,
							Issue.SEVERITY_HIGH, null);

					continue; // No need to check more if it's empty
				}
			}
			// Check for empty source when target is not empty, if requested
			if (getParams().getEmptySource()) {
				if (srcSeg.text.isEmpty() && !trgSeg.text.isEmpty()) {
					reportIssue(IssueType.EMPTY_SOURCESEG, tu, srcSeg.getId(),
							"The target segment is not empty, but its source is empty.", 0, -1, 0, -1,
							Issue.SEVERITY_HIGH, srcSeg.toString(), "", null);
					addAnnotation(srcCont, srcSeg.getId(), IssueType.EMPTY_SOURCESEG,
							"The target segment is not empty, but its source is empty.", 0, -1, 0, -1,
							Issue.SEVERITY_HIGH, null);
					continue; // No need to check more if the source is empty
				}
			}

			// Compile the patterns
			List<PatternItem> patterns = getParams().getPatterns();
			for (PatternItem item : patterns) {
				if (item.enabled) {
					item.compile();
				}
			}

			// Check for target is the same as source, if requested
			if (getParams().getTargetSameAsSource()) {
				if (getParams().getTargetSameAsSourceForSameLanguage() || !getSrcLoc().sameLanguageAs(getTrgLoc())) {
					if (hasMeaningfullText(srcSeg.text)) {
						if (srcSeg.text.compareTo(trgSeg.text, getParams().getTargetSameAsSourceWithCodes()) == 0) {
							// Is the string of the cases where target should be
							// the
							// same? (URL, etc.)
							boolean warn = true;
							if (patterns != null) {
								for (PatternItem item : patterns) {
									String ctext = srcSeg.text.getCodedText();
									if (item.enabled && item.target.equals(PatternItem.SAME)) {
										Matcher m = item.getSourcePattern().matcher(ctext);
										if (m.find()) {
											warn = !ctext.equals(m.group());
											break;
										}
									}
								}
							}
							if (warn) {
								reportIssue(IssueType.TARGET_SAME_AS_SOURCE, tu, srcSeg.getId(),
										"Translation is the same as the source.", 0, -1, 0, -1, Issue.SEVERITY_MEDIUM,
										srcSeg.toString(), trgSeg.toString(), null);
								addAnnotation(srcCont, srcSeg.getId(), IssueType.TARGET_SAME_AS_SOURCE,
										"Translation is the same as the source.", 0, -1, 0, -1, Issue.SEVERITY_MEDIUM,
										null);
							}
						}
					}
				}
			}

			// Check all suspect patterns
			checkSuspectPatterns(srcSeg, trgSeg, tu);
		}
		
		// Check for orphan target segments
		for (Segment trgSeg : trgSegs) {
			Segment srcSeg = srcSegs.get(trgSeg.getId());
			if (srcSeg == null) {
				reportIssue(IssueType.EXTRA_TARGETSEG, tu, trgSeg.getId(),
						String.format("Extra target segment (id=%s).", trgSeg.getId()),
						0, -1, 0, -1, Issue.SEVERITY_HIGH, "", trgSeg.toString(), null);
				addAnnotation(trgCont, trgSeg.getId(), IssueType.EXTRA_TARGETSEG,
						String.format("Extra target segment (id=%s).", trgSeg.getId()),
						0, -1, 0, -1, Issue.SEVERITY_HIGH, null);
				continue; // Cannot go further for that segment
			}
		}

		String srcOri = null;
		if (srcCont.contentIsOneSegment()) {
			srcOri = srcCont.toString();
		} else {
			srcOri = srcCont.getUnSegmentedContentCopy().toText();
		}

		String trgOri = null;
		if (trgCont.contentIsOneSegment()) {
			trgOri = trgCont.toString();
		} else {
			trgOri = trgCont.getUnSegmentedContentCopy().toText();
		}
		checkWhiteSpaces(srcOri, trgOri, tu);
		
		setAnnotationIds(srcCont, trgCont);
	}

	/**
	 * Indicates if we have at least one character that is part of the character
	 * set for a "word". digits are considered part of a "word".
	 * 
	 * @param frag
	 *            the text fragment to look at.
	 * @return true if a "word" is detected.
	 */
	private boolean hasMeaningfullText(TextFragment frag) {
		return WORDCHARS.matcher(frag.getCodedText()).find();
	}

	private boolean isSpaceWeCareAbout(char c) {
		return Character.isWhitespace(c) || Character.isSpaceChar(c);
	}

	private void checkWhiteSpaces(String srcOri, String trgOri, ITextUnit tu) {
		// Check for leading whitespaces
		if (getParams().getLeadingWS()) {

			// Missing ones
			for (int i = 0; i < srcOri.length(); i++) {
				if (isSpaceWeCareAbout(srcOri.charAt(i))) {
					if (srcOri.length() > i) {
						if ((trgOri.length() - 1 < i) || (trgOri.charAt(i) != srcOri.charAt(i))) {
							reportIssue(IssueType.MISSINGORDIFF_LEADINGWS, tu, null,
									String.format("Missing or different leading white space at position %d.", i), i,
									i + 1, 0, -1, Issue.SEVERITY_LOW, srcOri, trgOri, null);							
							addAnnotation(tu.getSource(), null, IssueType.MISSINGORDIFF_LEADINGWS,
									String.format("Missing or different leading white space at position %d.", i), i,
									i + 1, 0, -1, Issue.SEVERITY_LOW, null);
							break;
						}
					} else {
						reportIssue(IssueType.MISSING_LEADINGWS, tu, null,
								String.format("Missing leading white space at position %d.", i), i, i + 1, 0, -1,
								Issue.SEVERITY_LOW, srcOri, trgOri, null);
						addAnnotation(tu.getSource(), null, IssueType.MISSINGORDIFF_LEADINGWS,
								String.format("Missing leading white space at position %d.", i), i, i + 1, 0, -1,
								Issue.SEVERITY_LOW, null);
					}
				} else {
					break;
				}
			}

			// Extra ones
			for (int i = 0; i < trgOri.length(); i++) {
				if (isSpaceWeCareAbout(trgOri.charAt(i))) {
					if (srcOri.length() > i) {
						if ((srcOri.length() - 1 < i) || (srcOri.charAt(i) != trgOri.charAt(i))) {
							reportIssue(IssueType.EXTRAORDIFF_LEADINGWS, tu, null,
									String.format("Extra or different leading white space at position %d.", i), 0, -1,
									i, i + 1, Issue.SEVERITY_LOW, srcOri, trgOri, null);
							addAnnotation(tu.getTarget(getTrgLoc()), null, IssueType.EXTRAORDIFF_LEADINGWS,
									String.format("Extra or different leading white space at position %d.", i), 0, -1,
									i, i + 1, Issue.SEVERITY_LOW, null);
							break;
						}
					} else {
						reportIssue(IssueType.EXTRA_LEADINGWS, tu, null,
								String.format("Extra leading white space at position %d.", i), 0, -1, i, i + 1,
								Issue.SEVERITY_LOW, srcOri, trgOri, null);
						addAnnotation(tu.getTarget(getTrgLoc()), null, IssueType.EXTRA_LEADINGWS,
								String.format("Extra leading white space at position %d.", i), 0, -1, i, i + 1,
								Issue.SEVERITY_LOW, null);
					}
				} else {
					break;
				}
			}
		}

		// Check for trailing whitespaces
		if (getParams().getTrailingWS()) {
			// Missing ones
			int j = trgOri.length() - 1;
			for (int i = srcOri.length() - 1; i >= 0; i--) {
				if (isSpaceWeCareAbout(srcOri.charAt(i))) {
					if (j >= 0) {
						if ((trgOri.length() - 1 < j) || (trgOri.charAt(j) != srcOri.charAt(i))) {
							reportIssue(IssueType.MISSINGORDIFF_TRAILINGWS, tu, null,
									String.format("Missing or different trailing white space at position %d", i), i,
									i + 1, 0, -1, Issue.SEVERITY_LOW, srcOri, trgOri, null);
							addAnnotation(tu.getSource(), null, IssueType.MISSINGORDIFF_TRAILINGWS,
									String.format("Missing or different trailing white space at position %d", i), i,
									i + 1, 0, -1, Issue.SEVERITY_LOW, null);
							break;
						}
					} else {
						reportIssue(IssueType.MISSING_TRAILINGWS, tu, null,
								String.format("Missing trailing white space at position %d.", i), i, i + 1, 0, -1,
								Issue.SEVERITY_LOW, srcOri, trgOri, null);
						addAnnotation(tu.getSource(), null, IssueType.MISSING_TRAILINGWS,
								String.format("Missing trailing white space at position %d.", i), i, i + 1, 0, -1, 
								Issue.SEVERITY_LOW, null);
					}
				} else {
					break;
				}
				j--;
			}

			// Extra ones
			j = srcOri.length() - 1;
			for (int i = trgOri.length() - 1; i >= 0; i--) {
				if (isSpaceWeCareAbout(trgOri.charAt(i))) {
					if (j >= 0) {
						if ((srcOri.length() - 1 < j) || (srcOri.charAt(j) != trgOri.charAt(i))) {
							reportIssue(IssueType.EXTRAORDIFF_TRAILINGWS, tu, null,
									String.format("Extra or different trailing white space at position %d.", i), 0, -1,
									i, i + 1, Issue.SEVERITY_LOW, srcOri, trgOri, null);
							addAnnotation(tu.getTarget(getTrgLoc()), null, IssueType.EXTRAORDIFF_TRAILINGWS,
									String.format("Extra or different trailing white space at position %d.", i), 0, -1,
									i, i + 1, Issue.SEVERITY_LOW, null);
							break;
						}
					} else {
						reportIssue(IssueType.EXTRA_TRAILINGWS, tu, null,
								String.format("Extra white trailing space at position %d.", i), 0, -1, i, i + 1,
								Issue.SEVERITY_LOW, srcOri, trgOri, null);
						addAnnotation(tu.getTarget(getTrgLoc()), null, IssueType.EXTRA_TRAILINGWS,
								String.format("Extra white trailing space at position %d.", i), 0, -1, i, i + 1, 
								Issue.SEVERITY_LOW, null);
					}
				} else {
					break;
				}
				j--;
			}
		}
	}

	private void checkSuspectPatterns(Segment srcSeg, Segment trgSeg, ITextUnit tu) {
		String trgCText = trgSeg.text.getCodedText();

		if (getParams().getDoubledWord()) {
			Matcher m = patDoubledWords.matcher(trgCText);
			while (m.find()) {
				// Check against the exceptions
				// Use the lowercase of the word enclosed in ';' to match
				// against the list
				if (doubledWordExceptions.indexOf(";" + m.group(1).toLowerCase() + ";") == -1) {
					// Not in the list: Not an exception, so we report it
					reportIssue(IssueType.SUSPECT_PATTERN, tu, srcSeg.getId(),
							String.format("Double word: \"%s\" found in the target.", m.group()), 0, -1,
							TextFragment.fromFragmentToString(trgSeg.text, m.start()),
							TextFragment.fromFragmentToString(trgSeg.text, m.end()), Issue.SEVERITY_HIGH,
							srcSeg.toString(), trgSeg.toString(), null);
					addAnnotation(tu.getTarget(getTrgLoc()), null, IssueType.SUSPECT_PATTERN,
							String.format("Double word: \"%s\" found in the target.", m.group()),  0, -1,
							TextFragment.fromFragmentToString(trgSeg.text, m.start()),
							TextFragment.fromFragmentToString(trgSeg.text, m.end()), 
							Issue.SEVERITY_LOW, null);
				}
			}
		}
	}
}
