/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.languagetool;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.languagetool.Language;
import org.languagetool.language.Contributor;
import org.languagetool.rules.CommaWhitespaceRule;
import org.languagetool.rules.DoublePunctuationRule;
import org.languagetool.rules.MultipleWhitespaceRule;
import org.languagetool.rules.Rule;
import org.languagetool.rules.SentenceWhitespaceRule;
import org.languagetool.rules.UppercaseSentenceStartRule;
import org.languagetool.rules.patterns.PatternRule;
import org.languagetool.tokenizers.WordTokenizer;

/**
 * Default {@link Language} used when no other LanguageTool language is
 * available. Uses default {@link WordTokenizer} and a small list of style
 * rules.
 * 
 * @author jimh
 *
 */
public class DefaultLanguage extends Language {

	@Override
	public String getShortName() {
		return "default";
	}

	@Override
	public String getName() {
		return "Default";
	}

	@Override
	public String[] getCountries() {
		return new String[] {};
	}

	@Override
	public Contributor[] getMaintainers() {
		return new Contributor[] {};
	}

	@Override
	public List<Rule> getRelevantRules(ResourceBundle messages) throws IOException {
		return Arrays.asList(new CommaWhitespaceRule(messages), new DoublePunctuationRule(messages),
				new UppercaseSentenceStartRule(messages, this), new MultipleWhitespaceRule(messages, this),
				new SentenceWhitespaceRule(messages));
	}

	@Override
	protected synchronized List<PatternRule> getPatternRules() throws IOException {
		return Collections.emptyList();
	}
}
